package com.magnet.qe_mmx;

import android.test.ActivityTestCase;
import android.util.Log;

import com.magnet.mmx.MMXClient;
import com.magnet.mmx.client.*;

import java.util.List;

public class PubSubNodesTest extends ActivityTestCase {
    private static final String TAG = PubSubNodesTest.class.getSimpleName();
    private MMXClient mmxClient;
    private MMXListenerImpl mmxListener;
    private MMXClientConfigImpl mmxConfiguration;
    //private PubSubManager pubSubManager;

    private boolean isConnected = false;
    private boolean messageReceived = false;
    private static final String user1 = "pub_user1";
    private static final String user2 = "pub_user_two";
    private static final String topic1 = "test_topic1";
    private static final String topic2 = "test_topic2";

    public void setUp() {
        mmxConfiguration = new MMXClientConfigImpl();
        mmxListener= new MMXListenerImpl();
        mmxClient = MMXClient.getInstance(this.getInstrumentation().getTargetContext(), mmxConfiguration);

        if (mmxClient.isConnected()) {
            this.disconnect(true);
        }
    }

    private void customSleep() {
        try {
            for (int i=0; i<20; i++) {
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void waitThread() {
        final boolean[] done = {false};
        try {
            Thread waitThread = new Thread(new Runnable () {
                @Override
                public void run() {
                    synchronized (this) {
                        for (int i = 0; i < 20; i++) {
                            try {
                                Thread.currentThread().sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        notify();
                        done[0] = true;
                    }
                }
            });

            waitThread.start();

            synchronized (waitThread) {
                while (!done[0]) {
                    Log.d(TAG, "waiting for thread");
                    Thread.currentThread().sleep(100);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void connect(String user, boolean isAutoCreateUser) {
        mmxClient.connect(MMXClientConfigImpl.TEST_HOST_NAME, 5222, user, "pass".getBytes(), isAutoCreateUser, mmxListener, MMXClient.SecurityLevel.NONE);
        this.customSleep();
        assertEquals("AUTHENTICATION_SUCCESS", mmxListener.connectionEvent.toString());
        assertEquals(user, this.mmxClient.getConnectionInfo().username);
    }

    private void disconnect(boolean isComplete) {
        Log.d(TAG, "****************** Disconnect from the server");
        mmxClient.disconnect(isComplete);
        this.customSleep();
    }

    private void create_topic(String topic, TopicOptions options) throws MMXException {
        PubSubManager pubSubManager = mmxClient.getPubSubManager();
        pubSubManager.createTopic(topic, options);
    }

    private void delete_topic(String topic) {
        try {
            mmxClient.getPubSubManager().deleteTopic(topic);
        } catch (MMXException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void create_personal_topic(String topic, TopicOptions topicOptions) throws MMXException {
        PubSubManager pubSubManager = mmxClient.getPubSubManager();
        pubSubManager.createPersonalTopic(topic, topicOptions);
    }

    private void delete_personal_topic(String topic) {
        try {
            mmxClient.getPubSubManager().deletePersonalTopic(topic);
        } catch (MMXException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void cleanup_topics() throws MMXException {
        Log.d(TAG, "^^^^^^^^^^^^^^^^ begin cleanup_topics()");
        String login_user = mmxClient.getConnectionInfo().username;
        List<PubSubManager.MMXTopic> topics = null;

        try {
            topics = mmxClient.getPubSubManager().listTopics();
            if (topics.size() > 0) {
                for (PubSubManager.MMXTopic topic: topics) {
                    Log.d(TAG, "^^^^^^^ the topic: " + topic.getTopic() + "/" + topic.toString());
                    Log.d(TAG, "^^^^^^^ logged in user: " + topic.getUserId());

                    //if (login_user.equals(topic.getUserId())) {
                        if (topic.isPersonal()) {
                            Log.d(TAG, "^^^^^^^ personal topic to be deleted: " + topic.getTopic());
                            mmxClient.getPubSubManager().deletePersonalTopic(topic.getTopic());
                        } else {
                            Log.d(TAG, "^^^^^^^ application topic to be deleted: " + topic.getTopic());
                            mmxClient.getPubSubManager().deleteTopic(topic.getTopic());
                        }
                    //}
                }
            }
        } catch (MMXException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void publishApplicationTopicItems(String topic, Payload payload) throws MMXException {
        PubSubManager pubSubManager = mmxClient.getPubSubManager();
        pubSubManager.publish(topic, null, payload);
    }

    private void subscribeApplicationTopic(String topic) throws MMXException {
        PubSubManager pubSubManager = mmxClient.getPubSubManager();
        pubSubManager.subscribe(topic, true, true);
    }

    // Create Application topic topic1 - remain
    public void test01CreateApplicationTopic() throws MMXException {
        this.connect(user1, true);
        this.delete_topic(topic1);
        PubSubManager pubSubManager = mmxClient.getPubSubManager();

        int topicCount = pubSubManager.listTopics().size();
        TopicOptions topicOptions = new TopicOptions();

        try {
            Log.d(TAG, "^^^^^^^^^^ Begin createTopic");
            pubSubManager.createTopic(topic1, topicOptions);
        } catch (MMXException e) {
            Log.e(TAG, e.getMessage());
            fail();
        }

        assertEquals(topicCount+1, pubSubManager.listTopics().size());

    }

    // user1 create duplicated application topic topic1
    public void test02CreateDuplicateApplicationTopic() throws MMXException {
        this.connect(user1, true);
        PubSubManager pubSubManager = mmxClient.getPubSubManager();
        TopicOptions topicOptions = new TopicOptions();

        TopicExistsException topicExistsException = null;

        try {
            pubSubManager.createTopic(topic1, topicOptions);
        } catch(TopicExistsException e) {
            Log.d(TAG, "+++++++++++ topicExistsException was thrown correctly", topicExistsException);
            topicExistsException = e;
        }

        assertNotNull("TopicExistsException", topicExistsException);
    }

    // user1 create personal topic topic1 - remain
    public void test03CreatePersonalTopic() throws MMXException {
        this.connect(user1, true);
        this.delete_personal_topic(topic1);
        PubSubManager pubSubManager = mmxClient.getPubSubManager();
        int topicCount = pubSubManager.listTopics().size();
        TopicOptions topicOptions = new TopicOptions();

        try {
            pubSubManager.createPersonalTopic(topic1, topicOptions);
        } catch (MMXException e) {
            Log.e(TAG, e.getMessage());
            fail();
        }

        assertEquals(topicCount+1, pubSubManager.listTopics().size());
    }

    // user2 create personal topic topic1 - cleaned
    public void test04DifferentUserCreateDuplicatedPersonalTopic() throws MMXException {
        this.connect(user2, true);
        PubSubManager pubSubManager = mmxClient.getPubSubManager();
        int topicCount = pubSubManager.listTopics().size();
        TopicOptions topicOptions = new TopicOptions();
        try {
            pubSubManager.createPersonalTopic(topic1, topicOptions);
        } catch (MMXException e) {
            Log.e(TAG, e.getMessage());
            fail();
        }

        assertEquals(topicCount+1, pubSubManager.listTopics().size());
        this.delete_personal_topic(topic1);
    }

    // user1 publish item to application topic topic1
    public void test05PublishApplicationTopic() throws MMXException {
        this.connect(user1, true);
        PubSubManager pubSubManager = mmxClient.getPubSubManager();

        try {
            pubSubManager.publish(topic1, null, new Payload("text", "text", "First message from topic1"));
        } catch (MMXException e) {
            Log.d("+++++ mmxExption: ", e.getMessage());
            fail();
        }
    }

    // user2 subscribe to application topic topic1
    public void test06SubscribeApplicationTopic() throws MMXException {
        this.connect(user2, true);
        PubSubManager pubSubManager = mmxClient.getPubSubManager();

        pubSubManager.subscribe(topic1, false, true);

        this.customSleep();

        assertTrue("pubsub item received",
                "First message from topic1".equals(
                        mmxListener.pubsubItemReceiveMessage.getPayload().getDataAsString()));
    }

    // user2 unsubscribe application topic topic1
    public void test07UnsubscribeApplicationTopic() throws MMXException {
        this.connect(user2, true);
        PubSubManager pubSubManager = mmxClient.getPubSubManager();

        int subscribedTopicsCount = pubSubManager.listSubscribedTopics().size();
        Log.d("******* number of subscriptions before: ", ""+subscribedTopicsCount);
        for (PubSubManager.MMXTopic topic: pubSubManager.listSubscribedTopics()) {
            if (topic.getTopic().equals(topic1)) {
                try {
                    pubSubManager.unsubscribe(
                            topic.getTopic(),
                            pubSubManager.getSubscription(topic.getTopic()).getId()
                    );
                } catch (MMXException e) {
                    Log.d(TAG, e.getMessage());
                }
            }
        }

        assertEquals(subscribedTopicsCount-1, pubSubManager.listSubscribedTopics().size());
    }

    public void test08SubscribeMultipleApplicationTopics() throws MMXException {
        this.connect(user1, true);
        this.delete_topic(topic1);

        this.create_topic(topic1, new TopicOptions());
        this.create_topic(topic2, new TopicOptions());

        this.disconnect(true);

        this.connect(user2, true);
        this.subscribeApplicationTopic(topic1);
        this.subscribeApplicationTopic(topic2);

        this.disconnect(true);

        this.connect(user1, true);
        this.publishApplicationTopicItems(topic1, new Payload("text", "text", "First message from topic1"));
        this.publishApplicationTopicItems(topic2, new Payload("text", "text", "First message from topic2"));

        this.disconnect(true);
        this.connect(user2, true);
        customSleep();
        int itemsReceived = mmxListener.pubsubItemReceiveMessageList.size();

        this.disconnect(true);
        this.connect(user1, true);
        this.delete_topic(topic1);
        this.delete_topic(topic2);

        assertEquals(2, itemsReceived);

    }

    
/*
    private void cleanupTopics() throws MMXException {
        List<PubSubManager.MMXTopic> topics = null;
        try {
            topics = mmxClient.getPubSubManager().listTopics();
            Log.d("^^^^^^^^^^^^ number of topics: ", ""+topics.size());
            if (topics.size() > 0) {
                for (PubSubManager.MMXTopic topic: topics) {
                    Log.d("^^^^^ topic details: ", topic.getTopic());
                    mmxClient.getPubSubManager().deleteTopic(topic.getTopic(), topic.isPersonal());
                }
            }
        } catch (MMXException e) {
            Log.e(TAG, "MMXExepction for listTopics", e);
        }
    }

    private void cleanupSubscriptions() throws MMXException {
        try {
            List<PubSubManager.MMXSubscription> subs = mmxClient.getPubSubManager().listSubscriptions(null);
            for (PubSubManager.MMXSubscription sub : subs) {
                try {
                    mmxClient.getPubSubManager().unsubscribe(sub.getTopic(), sub.getId());
                } catch (MMXException e) {
                    Log.d(TAG, "unexpected exception un-subscribing from topics");
                }
            }
        } catch(MMXException e) {
            Log.e(TAG, "unexpected exception getting list of subscriptions", e);
        }
    }

    private void waitThread() {
        final boolean[] done = {false};
        try {
            Thread waitThread = new Thread(new Runnable () {
                @Override
                public void run() {
                    synchronized (this) {
                        for (int i = 0; i < 20; i++) {
                            try {
                                Thread.currentThread().sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        notify();
                        done[0] = true;
                    }
                }
            });

            waitThread.start();

            synchronized (waitThread) {
                while (!done[0]) {
                    Log.d(TAG, "waiting for thread");
                    Thread.currentThread().sleep(100);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void test01CreatePersistentTopic() throws MMXException {
        this.connect(user1, true);
        this.cleanupTopics();
        PubSubManager pubSubManager = mmxClient.getPubSubManager();
        List<PubSubManager.MMXTopic> topics = null;

        try {
            //pubSubManager.createTopic(topic1, new TopicOptions().setLogEnabled(true).setMaxItems(5));
            pubSubManager.createTopic(topic1, new TopicOptions().setMaxItems(5));
            topics = pubSubManager.listTopics();
            assertEquals(topics.size(), 1);
            assertTrue(topics.get(0).getTopic().equals(topic1));
        } catch (MMXException e) {
            Log.e(TAG, "MMXException for createTopic", e);
            fail("unexpected exception" + e);
        }
    }

    public void test02CreateTransientTopics() throws MMXException {
        this.connect(user1, true);
        PubSubManager pubSubManager = mmxClient.getPubSubManager();
        List<PubSubManager.MMXTopic> topics = null;

        try {
            pubSubManager.createTopic(topic2, new TopicOptions().setPersistent(false));
            topics = pubSubManager.listTopics();
            assertEquals(topics.size(), 2);
            assertTrue(topics.get(1).getTopic().equals(topic2));
        } catch (MMXException e) {
            Log.e(TAG, "MMXException for createTopic", e);
            fail("unexpected exception" + e);
        }

    }

    public void test03CreateDuplicatedTopic() throws MMXException {
        this.connect(user1, true);
        TopicExistsException topicExistsException = null;
        PubSubManager pubSubManager = mmxClient.getPubSubManager();
        // Test topicExistException thrown when creating a duplicated topic
        try {
            pubSubManager.createTopic(topic1, new TopicOptions());
        } catch (TopicExistsException e) {
            assertTrue(true);
            topicExistsException = e;
        }
        assertNotNull("failed throwing TopicExistsException when creating duplicated topic", topicExistsException);
        Log.d(TAG, ">>>>>> topicExistException was thrown correctly", topicExistsException);
    }

    public void test04PublishTopic() throws MMXException {
        this.connect(user1, true);
        PubSubManager pubSubManager = mmxClient.getPubSubManager();
        try {
            pubSubManager.publish(topic1, null, new Payload("text", "text", "First message from topic1"));
            pubSubManager.publish(topic1, null, new Payload("text", "text", "Second message from topic1"));
            pubSubManager.publish(topic1, null, new Payload("text", "text", "Third message from topic1"));

            pubSubManager.publish(topic2, null, new Payload("text", "text", "First message from topic2"));
            pubSubManager.publish(topic2, null, new Payload("text", "text", "Second message from topic2"));
            pubSubManager.publish(topic2, null, new Payload("text", "text", "Last message from topic2"));
        } catch (MMXException e) {
            fail("publish exception encountered" + e);
        }
        mmxClient.disconnect();
    }

    public void test05SubscribeToPersistentTopic() throws MMXException {
        this.connect(user2, true);
        this.cleanupSubscriptions();

        PubSubManager pubSubManager = mmxClient.getPubSubManager();

        // subscribe to the topic
        try {
            pubSubManager.subscribe(topic1, true, true);
            assertEquals(1, pubSubManager.listSubscriptions(null).size());

            this.waitThread();

            assertEquals(1, mmxListener.pubsubReceivedMessage.size());
            assertEquals("Third message from topic1", mmxListener.pubsubReceivedMessage.get(0).getPayload().getDataAsString());

            List<MMXMessage> items = pubSubManager.getItems(topic1, pubSubManager.getSubscription(topic1).getId(), 5);
            assertEquals(5, items.size());
        } catch(MMXException e) {
            Log.e(TAG, "MMXException for subscribeTopic", e);
            fail();
        }
        mmxClient.disconnect();
        this.customSleep();
    }

    public void test06SubscriberReconnectToServer() throws MMXException {
        this.connect(user1, false);
        mmxClient.getPubSubManager().publish(topic1, null, new Payload("text", "text", "Another message from topic1"));
        mmxClient.disconnect();
        this.customSleep();

        this.connect(user2, false);
        assertEquals(1, mmxListener.pubsubReceivedMessage.size());
        assertEquals("Another message from topic1", mmxListener.pubsubReceivedMessage.get(0).getPayload().getDataAsString());
    }

    public void test07SubscribeToTransientTopic() throws MMXException {
        this.connect(user2, false);
        this.cleanupSubscriptions();

        PubSubManager pubSubManager = mmxClient.getPubSubManager();

        // subscribe to the topic
        try {
            pubSubManager.subscribe(topic2, true, true);
            assertEquals(1, pubSubManager.listSubscriptions(null).size());
            this.waitThread();
            mmxClient.disconnect();

        } catch(MMXException e) {
            Log.e(TAG, "MMXException for subscribeTopic", e);
            fail();
        }
    }

    public void test08SubscribeNonExistTopic() throws MMXException {
        this.connect(user2, false);
        PubSubManager pubSubManager = mmxClient.getPubSubManager();
        TopicNotFoundException topicNotFoundException = null;
        try {
            pubSubManager.subscribe("NonExistTopic", true, true);
            List<PubSubManager.MMXSubscription> subs = pubSubManager.listSubscriptions(null);
            for (PubSubManager.MMXSubscription sub: subs) {
                if (sub.getTopic().equals("NonExistTopic")) {
                    fail("successfully subscribe to a non existing topic");
                }
            }
        } catch (TopicNotFoundException e) {
            topicNotFoundException = e;
            assertTrue(true);
        }
        assertNotNull("failed throwing TopicNotFoundException when subscribe to non-existing topic", topicNotFoundException);
        //Log.d(TAG, ">>>>>>> topicNotFoundException was thrown correctly: ", topicNotFoundException);
    }

    public void test09UnSubscribeTopic() throws MMXException {
        this.connect(user2, false);
        PubSubManager pubSubManager = mmxClient.getPubSubManager();
        List<PubSubManager.MMXSubscription> subs = pubSubManager.listSubscriptions(null);
        assertTrue(subs.size() > 0);

        for (PubSubManager.MMXSubscription sub : subs) {
            if (sub.getTopic().equals(topic1)) {
                try {
                    pubSubManager.unsubscribe(topic1, sub.getId());
                } catch (MMXException e) {
                    Log.d(TAG, "<<<<<<<<<<<< unexpected exception un-subscribing from topic: " + topic1, e);
                    fail();
                }
            } else if (sub.getTopic().equals(topic2)) {
                try {
                    pubSubManager.unsubscribe(topic2, sub.getId());
                } catch (MMXException e) {
                    Log.d(TAG, "<<<<<<<<<<<< unexpected exception un-subscribing from topic: " + topic1, e);
                    fail();
                }
            }
        }

        subs = pubSubManager.listSubscriptions(null);
        assertEquals("failed un-subscribe from topic", subs.size(), 0);
    }

    public void test10DeleteNonExistingTopic() throws MMXException {
        this.connect(user1, false);
        PubSubManager pubSubManager = mmxClient.getPubSubManager();
        TopicNotFoundException topicNotFoundException = null;

        // Test TopicNotFoundException thrown when trying to delete non-existing topic
        try {
            pubSubManager.deleteTopic("TopicNotFound", true);
        } catch (TopicNotFoundException e) {
            assertTrue(true);
            topicNotFoundException = e;
        }
        assertNotNull(topicNotFoundException);
        //Log.d(TAG, ">>>>>> TopicNotFoundException was thrown correctly: ", topicNotFoundException);
    }

    public void test11DeleteTopic() throws MMXException {
        this.connect(user1, false);
        PubSubManager pubSubManager = mmxClient.getPubSubManager();

        // Test deleting topic
        List<PubSubManager.MMXTopic> topics= pubSubManager.listTopics();
        for (PubSubManager.MMXTopic topic: topics) {
            if (topic.equals(topic1)) {
                try {
                    pubSubManager.deleteTopic(topic.getTopic(), topic.isPersonal());
                } catch (MMXException e) {
                    Log.d(TAG, "failed to delete topic" + topic, e);
                    fail();
                }
            } else if (topic.equals(topic2)) {
                try {
                    pubSubManager.deleteTopic(topic.getTopic(), topic.isPersonal());
                } catch (MMXException e) {
                    Log.d(TAG, "failed to delete topic" + topic, e);
                    fail();
                }
            }
        }
    }*/
}