package com.magnet.qe_mmx;

import java.lang.Override;
import com.magnet.mmx.MMXClientConfig;

/**
 * Created by tnguyen on 10/7/14.
 */
public class MMXClientConfigImpl implements MMXClientConfig {
    private static final String APP_ID = "cuoi2dj1dnl";
    private static final String API_KEY = "84c1cf00-9f53-4e13-9baf-289d7ae9f58c";
    private static final String GCM_SENDERID = "524627119409";
    private static final String SERVER_USERID = "manager1magnetapi.com%dwui2887p89";
    private static final String GUESS_USER_SECRET = "-1cjpboj1lknxr";
    public static final String TEST_HOST_NAME = "207.135.69.242";

    @Override
    public String getAppId() {
        return APP_ID;
    }

    @Override
    public String getApiKey() {
        return API_KEY;
    }

    @Override
    public String getGcmSenderId() {
        return GCM_SENDERID;
    }

    @Override
    public String getServerUser() {
        return SERVER_USERID;
    }

    @Override
    public String getGuestSecret() {
        return GUESS_USER_SECRET;
    }
}
