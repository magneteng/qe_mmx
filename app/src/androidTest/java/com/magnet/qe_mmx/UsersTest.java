package com.magnet.qe_mmx;

import android.test.ActivityTestCase;
import android.util.Log;

import com.magnet.mmx.MMXClient;
import com.magnet.mmx.client.MMXException;

/**
 * Created by tnguyen on 10/20/14.
 */
public class UsersTest extends ActivityTestCase {
    private MMXClient mmxClient;
    private MMXListenerImpl mmxListener;
    private MMXClientConfigImpl mmxClientConfig;

    private String TAG = ConnectionTest.class.getSimpleName();

    private static final String invalidUser1 = "Abcde12345Abcde12345Abcde12345Abcde12345Abcde12345abc";
    private static final String invalidUser2 = "abc%123def";
    private static final String invalidUser3 = "cdf/123gh";
    private static final String validUser = "Abcd_123.5Abcde12345Abcde12345Abcde12345Abcde12345ab";

    private void custom_sleep(int second) {
        try {
            for (int i = 0; i < second*10; i++) {
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void connect(String user) {
        mmxClientConfig = new MMXClientConfigImpl();
        mmxListener = new MMXListenerImpl();
        mmxClient = MMXClient.getInstance(this.getInstrumentation().getTargetContext(), mmxClientConfig);

        mmxClient.connect(MMXClientConfigImpl.TEST_HOST_NAME, 5222, user, "test".getBytes(), true, mmxListener, MMXClient.SecurityLevel.NONE);
        this.custom_sleep(2);
    }

    private void disconnect() {
        mmxClient.disconnect(true);
        this.custom_sleep(2);
    }

    public void test01ConnectUser53Characters() throws MMXException {
        this.connect(invalidUser1);
        assertEquals("AUTHENTICATION_FAILURE", mmxListener.connectionEvent.toString());
    }

    public void test02ConnectUserInvalidCharacter1() throws MMXException {
        this.connect(invalidUser2);
        assertEquals("CONNECTION_FAILED", mmxListener.connectionEvent.toString());
    }

    public void test03ConnectUserInvalidCharacter2() throws MMXException {
        this.connect(invalidUser3);
        assertEquals("CONNECTION_FAILED", mmxListener.connectionEvent.toString());
    }

    public void test04ConnectValidUser() throws MMXException {
        this.connect(validUser);
        System.out.print("^^^^^^^^^^^^ " + mmxClient.isConnected());
        assertTrue("Failed connect valid user", mmxClient.isConnected());
    }
}
