package com.magnet.qe_mmx;

import android.test.ActivityTestCase;
import android.util.Log;

import com.magnet.mmx.MMXClient;
import com.magnet.mmx.client.MMXException;

/**
 * Created by tnguyen on 10/16/14.
 */
public class ConnectionTest extends ActivityTestCase {
    private static final String user1 = "pub_user1";
    private String TAG = ConnectionTest.class.getSimpleName();
    private MMXClient mmxClient;
    private MMXListenerImpl mmxListener;
    private MMXClientConfigImpl mmxClientConfig;

    public void setUp() {
        Log.d(TAG, "1. ****************** Connect to the server");
        connect(user1, true);
    }

    private void custom_sleep(int second) {
        try {
            for (int i = 0; i < second*10; i++) {
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Connect with specified user_name and default password to "pass"
    private void connect(String user, boolean isAutoCreateUser) {
        mmxClientConfig = new MMXClientConfigImpl();
        mmxListener = new MMXListenerImpl();
        mmxClient = MMXClient.getInstance(this.getInstrumentation().getTargetContext(), mmxClientConfig);

        mmxClient.connect(MMXClientConfigImpl.TEST_HOST_NAME, 5222, user, "pass".getBytes(), isAutoCreateUser, mmxListener, MMXClient.SecurityLevel.NONE);
        this.custom_sleep(2);
    }

    // Connect with specified user_name and specified password
    private void connect(String user, String password, boolean isAutoCreateUser) {
        mmxClientConfig = new MMXClientConfigImpl();
        mmxListener= new MMXListenerImpl();
        mmxClient = MMXClient.getInstance(this.getInstrumentation().getTargetContext(), mmxClientConfig);

        mmxClient.connect(MMXClientConfigImpl.TEST_HOST_NAME, 5222, user, password.getBytes(), isAutoCreateUser, mmxListener, MMXClient.SecurityLevel.NONE);
        this.custom_sleep(2);
    }

    // Disconnect from the server
    private void disconnect(boolean isComplete) {
        Log.d(TAG, "****************** Disconnect from the server");
        mmxClient.disconnect(isComplete);
        this.custom_sleep(2);
    }

    public void test00ConnectDifferentUsers() throws MMXException {
        mmxClient.disconnect();
        this.connect("user2", true);
    }

    // Test Server Connection
    public void test01ServerConnectAutoCreateUser() throws MMXException {
        assertTrue("Failed connection to the MMX server with isAutoCreateUser set to true", mmxClient.isConnected());
        assertEquals("AUTHENTICATION_SUCCESS", mmxListener.connectionEvent.toString());
        assertEquals(user1, mmxClient.getConnectionInfo().username);
    }

    public void test02ServerConnectNonExistentUser() throws MMXException {
        // Disconnect from the MMX server
        this.disconnect(true);
        mmxClient.connect(MMXClientConfigImpl.TEST_HOST_NAME, 5222, "nonexistuser", "anypassword".getBytes(), false, mmxListener, MMXClient.SecurityLevel.NONE);
        this.custom_sleep(2);
        assertEquals("AUTHENTICATION_FAILURE", mmxListener.connectionEvent.toString());
        assertFalse("Expect mmxClient.isConnected to be false", mmxClient.isConnected());
    }

    public void test03ServerConnectSavedCredentials() throws MMXException {
        this.disconnect(false);
        Log.d(TAG, "********* Attempted to connect to the server with previously known credentials");
        mmxClient.connect(mmxListener);
        this.custom_sleep(3);
        assertEquals("AUTHENTICATION_SUCCESS", mmxListener.connectionEvent.toString());
        assertEquals(user1, mmxClient.getConnectionInfo().username);
        assertTrue("failed to connect with saved credentials", mmxClient.isConnected());

    }

    public void test04ServerConnectWrongPassword() throws MMXException {
        this.disconnect(false);
        Log.d(TAG, "********* Attempted to connect to the server with wrong password");
        this.connect(user1, "wrongPassword", false);
        assertTrue("Expecting connection event to be AUTHENTICATION_FAILURE", mmxListener.connectionEvent.toString().equals("AUTHENTICATION_FAILURE"));
        assertFalse("Expecting isConnected() to be false", mmxClient.isConnected());
    }

    public void test05ServerConnectWrongUsername() throws MMXException {
        this.disconnect(false);
        Log.d(TAG, "********* Attempted to connect to the server with wrong username");
        this.connect("wronguser", "test", false);
        assertTrue("Expecting connection event to be AUTHENTICATION_FAILURE", mmxListener.connectionEvent.toString().equals("AUTHENTICATION_FAILURE"));
        assertFalse("Expecting isConnected() to be false", mmxClient.isConnected());
    }

    public void test06TestServerDisconnect() throws MMXException {
        this.disconnect(true);
        assertFalse("Failed disconnection from the MMX server", mmxClient.isConnected());
    }
}