package com.magnet.qe_mmx;

import android.util.Log;

import com.magnet.mmx.MMXClient;
import com.magnet.mmx.client.MMXMessage;
import com.magnet.mmx.client.PubSubManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tnguyen on 10/15/14.
 */
public class MMXListenerImpl implements MMXClient.MMXListener {
    private static final String TAG = MMXListenerImpl.class.getSimpleName();
    public MMXMessage pubsubItemReceiveMessage = null;
    public MMXClient.ConnectionEvent connectionEvent = null;
    public List<MMXMessage> pubsubItemReceiveMessageList = new ArrayList<MMXMessage>();

    @Override
    public void onConnectionEvent(MMXClient client, MMXClient.ConnectionEvent event) {
        Log.d(TAG, "connection event=" + event);
        this.connectionEvent = event;
    }

    @Override
    public boolean onMessageReceived(MMXClient client, MMXMessage message, boolean isReceiptRequested) {
        Log.d(TAG, "onMessageReceived message=" + message.toString());
        return true;
    }

    @Override
    public void onSendFailed(MMXClient client, MMXMessage message) {
        Log.d(TAG, "onSendFailed message=" + message.toString());
    }

    @Override
    public void onMessageDelivered(MMXClient client, String recipient, String messageId) {
        Log.d(TAG, "onMessageDelivered message=" + messageId.toString());
    }

    @Override
    public void onPubsubItemReceived(MMXClient mmxClient, PubSubManager.MMXTopic mmxTopic, MMXMessage mmxMessage) {
        Log.d(TAG, "^^^^^^^^^^ onPubsubItemReceived topic=" + mmxTopic.getTopic() + ";message=" + mmxMessage.getPayload().getDataAsString());
        this.pubsubItemReceiveMessage = mmxMessage;
        this.pubsubItemReceiveMessageList.add(mmxMessage);
    }
}
